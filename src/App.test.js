import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import App from "./App";

describe("App Component", () => {
  // test case 1
  it("check strong password", async () => {
    render(<App />);
    const input = screen.getByPlaceholderText("Enter password");
    const getButtonAction = screen.getByText("Submit");

    fireEvent.change(input, { target: { value: "a" } });
    fireEvent.click(getButtonAction);
    await waitFor(() => {
      const resultElement = screen.getByText((content, element) => {
        const hasText = (text) => element.textContent === text;
        return content.startsWith("Output:") && hasText(content);
      });

      expect(resultElement).toBeInTheDocument();

      const resultValue = parseInt(
        resultElement.textContent.split(":")[1].trim()
      );

      expect(resultValue).toBe(5);
    });
  });
  // test case 2
  it("check strong password", async () => {
    render(<App />);
    const input = screen.getByPlaceholderText("Enter password");
    const getButtonAction = screen.getByText("Submit");

    fireEvent.change(input, { target: { value: "aA1" } });
    fireEvent.click(getButtonAction);
    await waitFor(() => {
      const resultElement = screen.getByText((content, element) => {
        const hasText = (text) => element.textContent === text;
        return content.startsWith("Output:") && hasText(content);
      });

      expect(resultElement).toBeInTheDocument();

      const resultValue = parseInt(
        resultElement.textContent.split(":")[1].trim()
      );

      expect(resultValue).toBe(3);
    });
  });
  // test case 3
  it("check strong password", async () => {
    render(<App />);
    const input = screen.getByPlaceholderText("Enter password");
    const getButtonAction = screen.getByText("Submit");

    fireEvent.change(input, { target: { value: "1337COd3" } });
    fireEvent.click(getButtonAction);
    await waitFor(() => {
      const resultElement = screen.getByText((content, element) => {
        const hasText = (text) => element.textContent === text;
        return content.startsWith("Output:") && hasText(content);
      });

      expect(resultElement).toBeInTheDocument();

      const resultValue = parseInt(
        resultElement.textContent.split(":")[1].trim()
      );

      expect(resultValue).toBe(0);
    });
  });
});
