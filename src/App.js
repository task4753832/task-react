import React, { useState } from "react";
import "./App.css";

function App() {
  const [inputPassword, setInputPassword] = useState("");
  const [passwordResult, setPasswordResult] = useState("");

  const PasswordSubmit = async () => {
    const response = await fetch(
      "http://localhost:3002/api/password-strength",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ password: inputPassword }),
      }
    );

    const data = await response.json();
    console.log(data);
    setPasswordResult(data.result);
  };

  return (
    <div
      className="App"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
        padding: "20px",
        backgroundColor: "#f2f5fd",
      }}
    >
      <div
        style={{
          margin: "20px 0",
          padding: "50px",
          border: "1px solid #ccc",
          borderRadius: "5px",
          backgroundColor: "#ffffff",
          boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
        }}
      >
        <h2> Check Password Strength</h2>
        <input
          type="text"
          placeholder="Enter password"
          name="password"
          value={inputPassword}
          onChange={(e) => setInputPassword(e.target.value)}
          style={{
            width: "100%",
            padding: "10px",
            border: "1px solid #ccc",
            borderRadius: "3px",
            backgroundColor: "#f8f8f8",
            fontFamily: "Arial, sans-serif",
          }}
        />
        <button
          onClick={PasswordSubmit}
          style={{
            marginTop: "10px",
            padding: "10px 15px",
            border: "none",
            borderRadius: "3px",
            backgroundColor: "#007bff",
            color: "#ffffff",
            cursor: "pointer",
            transition: "background-color 0.2s",
          }}
        >
          Submit
        </button>
        {passwordResult !== "" && (
          <p style={{ marginTop: "10px" }}>Output: {passwordResult}</p>
        )}
      </div>
    </div>
  );
}

export default App;
